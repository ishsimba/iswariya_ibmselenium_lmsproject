package lmsBatch4;

import org.openqa.selenium.By;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/*
 * User : Iswariya
 Complete a topic in a lesson
Goal: Navigate to a particular lesson and complete a topic in it.
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Find the navigation bar.
c.	Select the menu item that says �All Courses� and click it.
d.	Select any course and open it.
e.	Click on a lesson in the course. Verify the title of the course.
f.	Open a topic in that lesson. Mark it as complete.
g.	Mark all the topics in the lesson as complete (if available).
h.	Close the browser.




 */

public class Scenario10 {


  WebDriver driver;
  boolean flag;
		
        @Test
		public void login() {
			driver.findElement(By.linkText("My Account")).click();
			driver.findElement(By.xpath("//a[contains(@class,'ld-button')]")).click();
			Reporter.log("Navigated to Login page |");
			driver.findElement(By.id("user_login")).sendKeys("root");
			driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
			driver.findElement(By.id("wp-submit")).click();
		}
	  @Test(dependsOnMethods = { "login" })
		  public void socialMediaMarketing()
		  {
			// Navigates to All Courses
					driver.findElement(By.linkText("All Courses")).click();
					// Navigates to Social Media Marketing
					driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click();
					//Get the title of the page
					String mainpage=driver.getTitle();
					//Print the title of the page
					System.out.println("The Title of the Main page is: " +mainpage);
					//Verifying the title of the page
					Assert.assertEquals(mainpage, "Social Media Marketing � Alchemy LMS");
					//Navigate to Topic Monitoring & Digital Advertising
					driver.findElement(By.xpath("//div[contains(text(),'Monitoring')]")).click();
					//Navigate to the lesson Success with Advert
					driver.findElement(By.xpath("//span[contains(text(),'Success')]")).click();
					//Check whether Mark complete button is available					
					flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
					
					if (flag)
					{
						// clicks the mark complete button
						clickMarkComplete();
					}
					
					else
					{
						// Navigate to Next topic - Relationships
						driver.findElement(By.xpath("(//span[text()='Next Topic'])[1]")).click();
						// Checks whether the mark complete button is available
						flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
						if (flag)
						{
							// clicks the mark complete button
							clickMarkComplete();
						}
						else
						{
							//Display the message that the lesson is already marked as complete
							System.out.println("The Lesson is in complete status");
							
						}
					}
					
		  }
					// This method calls whenever the user want to click mark complete button across
					// the page
					private void clickMarkComplete() {
						WebElement complete = driver.findElement(By.xpath("(//input[@value='Mark Complete'])[1]"));
						if (complete.isDisplayed()) {
							complete.click();
						}
					
					}

					// Checks whether the Element present or not
					protected boolean isElementPresent(By by) {
						try {
							driver.findElement(by);
							return true;
						} catch (NoSuchElementException e) {
							return false;
						}
					}
		  @BeforeTest
			public void beforeTest() {

				System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
				driver = new FirefoxDriver();
				driver.get("https://alchemy.hguy.co/lms");
				driver.manage().window().maximize();
				driver.manage().deleteAllCookies();

			}
		  @AfterClass
		  public void afterClass() {
			  driver.close();
		  }

}
