package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

/*
* User : Iswariya
* Navigate to another page
Goal: Navigate to the �My Account� page on the site.
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Find the navigation bar.
c.	Select the menu item that says �My Account� and click it.
d.	Read the page title and verify that you are on the correct page.
e.	Close the browser.


*/

public class Scenario5 {
	WebDriver driver;
  @Test
  public void accountNavigation() 
  {
	  //Navigate to account page
	  driver.findElement(By.linkText("My Account")).click();
	  String title=driver.getTitle();
	  //Verify the title of the Account Page
	  Assert.assertEquals(title, "My Account � Alchemy LMS");
	//Print the title of the Account Page
	  System.out.println("The Title of the page is:" + title);
	  
	  
  }
  
  @BeforeTest
  public void beforeTest() 
  {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
  }

  @AfterTest
  public void afterTest() 
  {
	  driver.close();
  }

}
