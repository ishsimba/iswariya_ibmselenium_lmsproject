package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

/*
* User : Iswariya
* Verify the title of the first info box
Goal: Read the title of the first info box on the website and verify the text
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Get the title of the first info box.
c.	Make sure it matches �Actionable Training� exactly.
d.	If it matches, close the browser.



*/

public class Scenario3 {
	WebDriver driver;
  @Test
  public void validateFirstinfobox() 
  {
	 String firstboxtitle= driver.findElement(By.xpath("//h3[contains(@class,'uagb-ifb-title')]")).getText();
	 //verify the title of the first information box
	 Assert.assertEquals(firstboxtitle,"Actionable Training");
	//Print the title of the first information box
	 System.out.println("The title of the first info box: " +firstboxtitle);
  }
  @BeforeTest
  public void beforeTest()
  {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
  }

  @AfterTest
  public void afterTest()
  {
	  driver.close();
  }

}
