package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

/*
 * User : Iswariya
 * Count the number of courses
Goal: Navigate to the �All Courses� page and count the number of courses.
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Find the navigation bar.
c.	Select the menu item that says �All Courses� and click it.
d.	Find all the courses on the page.
e.	Print the number of courses on the page

 */

public class Scenario7 {
	WebDriver driver;
  @Test
  public void allCourses() 
  {
	  driver.findElement(By.linkText("All Courses")).click();
	  List <WebElement> course=driver.findElements(By.xpath("//div[@class='ld_course_grid col-sm-8 col-md-4 ']"));
	  int count=course.size();
	  //Print the number of course
	  System.out.println("The Number of courses in the page : " + count);
	   
  }
	
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
