package lmsBatch4;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/*
 * User : Iswariya
 Use XPath to find an element on the page
Goal: Using XPath to find elements on the page and complete a lesson.
 .	Open the browser to the All Courses page of Alchemy LMS site.
a.	Find a course to open using XPath notations.
b.	Find a lesson in that course and open it using XPath notations.
c.	Find the Mark Complete button on the page using XPath and click it.
d.	Check progress of course by finding the progress bar on the page using XPath
e.	Close the browser

 */

public class Scenario12 {
	WebDriver driver;
	  boolean flag;
			
	        @Test
			public void login() {
				driver.findElement(By.xpath("//a[text()='My Account']")).click();
				driver.findElement(By.xpath("//a[contains(@class,'ld-button')]")).click();
				Reporter.log("Navigated to Login page |");
				driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
				driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
				driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
			}
		  @Test(dependsOnMethods = { "login" })
			  public void socialMediaMarketing()
			  {
				// Navigates to All Courses
						driver.findElement(By.xpath("//a[text()='All Courses']")).click();
						// Navigates to Social Media Marketing
						driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click();
						//Get the title of the page
						String mainpage=driver.getTitle();
						//Print the title of the page
						System.out.println("The Title of the Main page is: " +mainpage);
						//Verifying the title of the page
						Assert.assertEquals(mainpage, "Social Media Marketing � Alchemy LMS");
						//Navigate to Investment & Marketing Final Strategies
						driver.findElement(By.xpath("//span[text()='Expand All']")).click();
						
						driver.findElement(By.xpath("//span[contains(text(),'This is')]")).click();
						String lessontitle=driver.getTitle();
						System.out.println("The title of the lesson is: " +lessontitle);
						flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));

						if (flag)
						{
							// clicks the mark complete button
							clickMarkComplete();
						}
						
						else 
						{
							System.out.println("The lesson is in complete status");
						}
						
  
}
		  private void clickMarkComplete() {
				WebElement complete = driver.findElement(By.xpath("(//input[@value='Mark Complete'])[1]"));
				if (complete.isDisplayed()) {
					complete.click();
				}
			}

			// Checks whether the Element present or not
			protected boolean isElementPresent(By by) {
				try {
					driver.findElement(by);
					return true;
				} catch (NoSuchElementException e) {
					return false;
				}
			}
		  @BeforeTest
			public void beforeTest() {

				System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
				driver = new FirefoxDriver();
				driver.get("https://alchemy.hguy.co/lms");
				driver.manage().window().maximize();

			}
		  @AfterClass
		  public void afterClass() {
			  driver.close();
		  }
}
