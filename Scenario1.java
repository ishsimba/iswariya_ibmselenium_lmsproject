package lmsBatch4;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
/*
 * User : Iswariya
 * Verify the website title
Goal: Read the title of the website and verify the text
a.	Open a browser.
b.	Navigate to �https://alchemy.hguy.co/lms�. 
c.	Get the title of the website.
d.	Make sure it matches �Alchemy LMS � An LMS Application '' exactly.
e.	If it matches, close the browser.

 */
public class Scenario1 {
	
	WebDriver driver;
  @Test
    public void validatetitle()
  {
	  String title=driver.getTitle();
	  //Verify the website title
	  Assert.assertEquals(title, "Alchemy LMS � An LMS Application");  
	  //Print the title of the website
	  System.out.println("The Title of the pate is: "+title);
  }
  @BeforeTest
  public void beforeTest() 
  {
	  
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  
	  
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
