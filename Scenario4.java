package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

/*
* User : Iswariya
* Verify the title of the second most popular course
Goal: Read the title of the second most popular course on the website and verify the text
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Get the title of the second most popular course.
c.	Make sure it matches �Email Marketing Strategies� exactly.
d.	If it matches, close the browser


*/

public class Scenario4 {
	WebDriver driver;
  @Test
  public void validateSecondInfobox() {
	  
	  String marketing=driver.findElement(By.xpath("//h3[text()='Email Marketing Strategies']")).getText();
	//verify the title of the second most popular course
	  Assert.assertEquals(marketing,"Email Marketing Strategies" );
	//Print the title of the second most popular course
	  System.out.println("Title of the second most popular course: "+marketing);
  }
  @BeforeTest
  public void beforeTest() 
  {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
  }

  @AfterTest
  public void afterTest()
  {
	  driver.close();
  }

}
