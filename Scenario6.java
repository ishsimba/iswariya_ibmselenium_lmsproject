package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;

/*
* User : Iswariya
* Logging into the site
Goal: Open the website and log-in using the provided credentials.
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Find the navigation bar.
c.	Select the menu item that says �My Account� and click it.
d.	Read the page title and verify that you are on the correct page.
e.	Find the �Login� button on the page and click it.
f.	Find the username field of the login form and enter the username into that field.
g.	Find the password field of the login form and enter the password into that field.
h.	Find the login button and click it.
i.	Verify that you have logged in.
j.	Close the browser.



*/

public class Scenario6 {
	
	WebDriver driver;
  @Test
  public void accountScreen() {
	  driver.findElement(By.linkText("My Account")).click();
	  Reporter.log("Navigated to My Account page |");
	  String title=driver.getTitle();
	  System.out.println("The Title of the page is:" + title);
	  Assert.assertEquals(title, "My Account � Alchemy LMS");
	  Reporter.log("Title of the My Account page is verified |");
  }
  @Test
  public void loginValidation()
  {
	  //Navigation for Login button
	  driver.findElement(By.xpath("//a[contains(@class,'ld-button')]")).click();
	  Reporter.log("Navigated to Login page |");
	  //Enter username
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  //Enter password
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  //Clicks submit
	  driver.findElement(By.id("wp-submit")).click();
	  
  }
  @BeforeTest
  public void beforeTest() 
  {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
  }

  @AfterTest
  public void afterTest() 
  {
	  //driver.close();
  }

}
