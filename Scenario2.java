package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

/*
* User : Iswariya
* Verify the website heading
Goal: Read the heading of the website and verify the text
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Get the heading of the webpage.
c.	Make sure it matches �Learn from Industry Experts� exactly.
d.	If it matches, close the browser.


*/

public class Scenario2 {
	WebDriver driver;
  @Test
  public void validateHeader() 
  {
	  String header=driver.findElement(By.xpath("//h1[contains(@class,'uagb-ifb-title')]")).getText();
	  //Verify the header of the Website
	  Assert.assertEquals(header, "Learn from Industry Experts");
	  //Print the header of the Website
	  System.out.println("The Header of the page is:" + header);
  }
  @BeforeClass
  public void beforeClass()
  {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
