package lmsBatch4;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/*
 * User : Iswariya
 Complete an entire course using only XPath
Goal: Finding elements on the page and using nothing but XPath notations to complete a course
 .	Open the browser to the All Courses page of Alchemy LMS site.
a.	Find a course to open using XPath notations.
b.	Find a lesson in that course and open it using XPath notations.
c.	Open each topic in the lesson using XPath and mark them as complete.
d.	Mark the lesson as complete and verify progress of the course.
e.	Close the browser.

 */

public class Scenario13 {
	WebDriver driver;
	  boolean flag;
			
	        @Test
			public void login() {
				driver.findElement(By.xpath("//a[text()='My Account']")).click();
				driver.findElement(By.xpath("//a[contains(@class,'ld-button')]")).click();
				Reporter.log("Navigated to Login page |");
				driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
				driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
				driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
			}
	        @Test(dependsOnMethods = { "login" })
	    	public void allCourses() throws IOException, InterruptedException

	    	{
	    		// Navigates to All Courses
	        	driver.findElement(By.xpath("//a[text()='All Courses']")).click();
	    		// Navigates to content Marketing
	    		driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[3]")).click();
	    		String coursename=driver.getTitle();
	    		System.out.println("The name of the course is: " +coursename);
	    		// Expands all the lessons
	    		driver.findElement(By.xpath("//div[@class='ld-expand-button ld-primary-background']")).click();

	    		// Navigates to Effective writing lesson
	    		driver.findElement(By.xpath("(//span[@class='ld-item-components'])[1]")).click();
	    		String topic1=driver.getTitle();
	    		System.out.println("The Title of the Topic-1 is: " + topic1);
	    		
	    		
	    		boolean flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
	    		
	    		if (flag) {
	    			System.out.println("Mark Complete is clicked");
	    			clickMarkComplete();
	    		} 
	    		else 
	    		{
	    			System.out.println("Effective Writing & Promoting Your Content is in complete status");
	    			// Clicks the next lesson page
	    			driver.findElement(By.xpath("(//span[@class='ld-text'])[1]")).click();
	    			String topic2=driver.getTitle();
	    			System.out.println("The Title of the Topic-2 is :" +topic2);

	    			// Navigates to Growth Hacking with your content lesson
	    			driver.findElement(By.xpath("//span[text()='Growth Hacking With Your Content']")).click();
	    			String lesson1=driver.getTitle();
	    			System.out.println("The Title of the Lesson 1 is: " + lesson1);
	    			flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
	    			if (flag) 
	    			{
	    				System.out.println("Mark Complete is clicked");
	    				clickMarkComplete();
	    			} 
	    			else
	    			{
	    				System.out.println("Growth Hacking With Your Content is in complete status");
	    				// Next Topic navigates to the Power of effective content
	    				driver.findElement(By.xpath("(//span[@class='ld-text'])[1]")).click();
	    				String lesson2=driver.getTitle();
	    				System.out.println("The Title of the Lesson 2 is:" +lesson2);
	    				// Checks whether the mark complete button is available
	    				flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));

	    				if (flag)
	    				{
	    					System.out.println("Mark Complete is clicked");
	    					// clicks the mark complete button
	    					clickMarkComplete();
	    				} 
	    				else 
	    				{
	    					System.out.println("Power of effective content is in complete status");
	    					// Navigates to back to lesson
	    					driver.findElement(By.xpath("(//a[text()='Back to Lesson'])")).click();
	    					flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
	    					// Final complete button
	    					if(flag)					
	    						clickMarkComplete();
	    					else
	    					{
	    						System.out.println("Analyze Content & Develop Writing Strategies is in complete status");
	    						
	    					}
	    				}
	    			}
	    		}
	    		String percentage=driver.findElement(By.xpath("//div[contains(@class,'ld-progress-percentage')]")).getText();
				System.out.println("Cource complete Status: " +percentage);

	    	}

	    	// This method calls whenever the user want to click mark complete button across
	    	// the page
	    	private void clickMarkComplete() {
	    		WebElement complete = driver.findElement(By.xpath("(//input[@value='Mark Complete'])[1]"));
	    		if (complete.isDisplayed()) {
	    			complete.click();
	    		}
	    	}

	    	// Checks whether the Element present or not
	    	protected boolean isElementPresent(By by)
	    	{
	    		try {
	    			driver.findElement(by);
	    			return true;
	    		} catch (NoSuchElementException e) {
	    			return false;
	    		}
	    	}
	    	@BeforeTest
			public void beforeTest() {

				System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
				driver = new FirefoxDriver();
				driver.get("https://alchemy.hguy.co/lms");
				driver.manage().window().maximize();

			}
		  @AfterClass
		  public void afterClass() {
			  //driver.close();
		  }

}
