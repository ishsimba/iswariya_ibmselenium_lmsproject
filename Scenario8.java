package lmsBatch4;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

/*
 * User : Iswariya
 *Contact the admin
Goal: Navigate to the �Contact Us� page and complete the form.
 .	Open a browser.
a.	Navigate to �https://alchemy.hguy.co/lms�. 
b.	Find the navigation bar.
c.	Select the menu item that says �Contact� and click it.
d.	Find the contact form fields (Full Name, email, etc.)
e.	Fill in the information and leave a message.
f.	Click submit.
g.	Read and print the message displayed after submission.
h.	Close the browser.


 */

public class Scenario8 {
	
	WebDriver driver;
	
  @Test
  public void contactForm() {
	  driver.findElement(By.xpath("//a[text()='Contact']")).click();
	  driver.findElement(By.xpath("//input[@id='wpforms-8-field_0']")).sendKeys("Iswariya");
	  driver.findElement(By.xpath("//input[@id='wpforms-8-field_1']")).sendKeys("iswari@gmail.com");
	  driver.findElement(By.xpath("//input[@id='wpforms-8-field_3']")).sendKeys("About course details");
	  driver.findElement(By.xpath("//textarea[@id='wpforms-8-field_2']")).sendKeys("Would like to join course");
	  driver.findElement(By.xpath("//button[@id='wpforms-submit-8']")).click();
	  String message=driver.findElement(By.xpath("//div[@id='wpforms-confirmation-8']")).getText();
	  //Prints the message displayed after submission
	  System.out.println(message);
  }
  @BeforeClass
  public void beforeClass() {
	  System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
	  driver=new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
