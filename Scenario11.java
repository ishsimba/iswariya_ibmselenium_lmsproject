package lmsBatch4;

import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/*
 * User : Iswariya
 Complete an entire course
Goal: Navigate to a particular course and complete all lessons and topics in it.
 .	Open the browser to the All Courses page of Alchemy LMS site.
a.	Select any course and open it.
b.	Click on a lesson in the course. Verify the title of the course.
c.	Open a topic in that lesson. Mark it as complete.
d.	Perform the above steps for all lessons and topics in the course.
e.	Verify the course is complete by checking the value of the progress bar.
f.	Close the browser.

 */

public class Scenario11
{
	WebDriver driver;
	boolean flag;
	
	@Test
	public void login() {
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.xpath("//a[contains(@class,'ld-button')]")).sendKeys(Keys.ENTER);
		Reporter.log("Navigated to Login page |");
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click();
	}
  @Test(dependsOnMethods = { "login" })
  public void emailMarketingStrategies()
  {
	// Navigates to All Courses
			driver.findElement(By.linkText("All Courses")).click();
			// Navigates to Email Marketing Strategies
			driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[2]")).click();
			//Get the title of the page
			String mainpage=driver.getTitle();
			System.out.println("The Title of the Main page is: " +mainpage);
			//Navigate to the option Deliverability Of Your Emails
			driver.findElement(By.xpath("(//div[@class='ld-item-title'])[1]")).click();
			String lesson1=driver.getTitle();
			System.out.println("The Title of the Lesson 1 is: " +lesson1);
			
			// Checks whether the mark complete button is available
			flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));

			if (flag)
			{
				// clicks the mark complete button
				clickMarkComplete();
			}
			
			else 
			{
				System.out.println(lesson1 + " is in complete status");
				// Navigate to Improving & Designing Marketing Emails
				driver.findElement(By.xpath("(//span[@class='ld-text'])[1]")).click();
				String lesson2=driver.getTitle();
				System.out.println("The Title of the Lesson 2 is: " +lesson2);
								// Checks whether the mark complete button is available
				flag = isElementPresent(By.xpath("(//input[@value='Mark Complete'])[1]"));
				if (flag)
				{
					// clicks the mark complete button
					clickMarkComplete();
				}
				else
					
				{
					System.out.println(lesson2 + " is in complete status");
					
				}
				
				
			}
			String percentage=driver.findElement(By.xpath("//div[contains(@class,'ld-progress-percentage')]")).getText();
			System.out.println("Cource complete Status: " +percentage);
			
  }
			
			// This method calls whenever the user want to click mark complete button across
			// the page
			private void clickMarkComplete() {
				WebElement complete = driver.findElement(By.xpath("(//input[@value='Mark Complete'])[1]"));
				if (complete.isDisplayed()) {
					complete.click();
				}
			}

			// Checks whether the Element present or not
			protected boolean isElementPresent(By by) {
				try {
					driver.findElement(by);
					return true;
				} catch (NoSuchElementException e) {
					return false;
				}
			}
  @BeforeTest
	public void beforeTest() {

		System.setProperty("webdriver.gecko.driver","C:\\Training\\selenium_java-master\\selenium_java-master\\Geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
		driver.manage().window().maximize();

	}
  @AfterClass
  public void afterClass()
  {
	  driver.close();
  }

  
}
